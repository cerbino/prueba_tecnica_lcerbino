﻿using System;
using System.Collections.Generic;
using SharpBank.Enums;

namespace SharpBank.Entity

{
    public class Customer
    {
        private readonly String _name;
        private readonly List<Account> _accounts;

        public Customer(String name)
        {
            this._name = name;
            this._accounts = new List<Account>();
        }

        public String GetName()
        {
            return _name;
        }

        public Customer OpenAccount(Account account)
        {
            _accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return _accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in _accounts)
                total += a.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + _name + "\n";
            double total = 0.0;
            foreach (Account a in _accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }


        public bool TransferBetweenAccounts(Account accountBase, Account accountDestiny, double amountOfMoney)
        {
            if (amountOfMoney > accountBase.SumTransactions())
                return false;

            accountBase.Withdraw(amountOfMoney);
            accountDestiny.Deposit(amountOfMoney);

            return true;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case (int)EnumTypeAccount.Checking:
                    s += "Checking Account\n";
                    break;
                case (int)EnumTypeAccount.Savings:
                    s += "Savings Account\n";
                    break;
                case (int)EnumTypeAccount.MaxiSavings:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a._transactions)
            {
                s += "  " + (t._amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t._amount) + "\n";
                total += t._amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        private string ToDollars(double d)
        {
            return $"${Math.Abs(d):N2}";
        }
    }
}
