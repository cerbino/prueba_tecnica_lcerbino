﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpBank.Enums;

namespace SharpBank.Entity
{
    public class Account
    {

        private readonly int _accountType;
        public List<Transaction> _transactions;

        public Account(int accountType)
        {
            this._accountType = accountType;
            this._transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                _transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                _transactions.Add(new Transaction(-amount));
            }
        }

        public double InterestEarned()
        {
            double amount = SumTransactions(true);

            switch (_accountType)
            {
                case (int)EnumTypeAccount.Savings:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
                case (int)EnumTypeAccount.MaxiSavings:
                    if (this.IsNotHavingWithDrawsLastTenDays())
                        return amount * 0.05;
                    return amount * 0.001;
                default:
                    return amount * 0.001;
            }
        }

        private double ApplyDiaryAcumulateInterest(double amount, double anualInterestYear)
        {
            double amountAccumulate = amount;
            double diaryInterest = anualInterestYear / Convert.ToDouble(365);

            for (int i= 0; i<365; i++)
            {
                amountAccumulate += (diaryInterest / Convert.ToDouble(100)) * amountAccumulate;
            }

            return amountAccumulate;
        }

        private bool IsNotHavingWithDrawsLastTenDays()
        {
            var listTransactionTenDays = _transactions.Where(x => x.TransactionDate.Day >= DateTime.Now.Day - 10 && x._amount <  0).ToList();

            return  listTransactionTenDays.Any();
        }

        public double SumTransactions( bool currentYear = false)
        {
            return CheckIfTransactionsExist(currentYear);
        }

        private double CheckIfTransactionsExist(bool currentYear)
        {
            if (currentYear)
                return _transactions.Where(x => x.TransactionDate.Year == DateTime.Now.Year).Sum(x => x._amount);
            else
                return _transactions.Sum(x => x._amount);
        }

        public int GetAccountType()
        {
            return _accountType;
        }

    }
}
