﻿using System;
using SharpBank.Provider;

namespace SharpBank.Entity
{
    public class Transaction
    { 
        public readonly double _amount;

        public DateTime TransactionDate { get; }

        public Transaction(double amount)
        {
            this._amount = amount;
            this.TransactionDate = DateProvider.GetInstance().Now();
        }

    }
}
