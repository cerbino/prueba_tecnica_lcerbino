﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Enums
{
    public enum EnumTypeAccount
    {
        Checking = 0,

        Savings = 1,

        MaxiSavings = 2
    }
}
