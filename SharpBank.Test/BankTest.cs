﻿using NUnit.Framework;
using SharpBank.Entity;
using SharpBank.Enums;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {

            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account((int)EnumTypeAccount.Savings));


            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)EnumTypeAccount.Checking);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount_WithMoreThanOneThounsand()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)EnumTypeAccount.Savings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount_WithMoreLessThanOneThounsand()
        {
            //arrange
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)EnumTypeAccount.Savings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            //act
            checkingAccount.Deposit(100.0);

            //Assert
            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }


        [Test]
        public void MaxiSavingsAccount_WithDefaultInterest()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)EnumTypeAccount.MaxiSavings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0);

            Assert.AreEqual(3.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount_WithWithdrawsLastTenDays()
        {
            //arrange
            Bank bank = new Bank();
            Account checkingAccount = new Account((int)EnumTypeAccount.MaxiSavings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));
            checkingAccount.Deposit(6000.0);
            checkingAccount.Withdraw(3000.0);
           
            //Act
            double interest = bank.TotalInterestPaid();

            //Assert
            Assert.AreEqual(150.0, interest, DOUBLE_DELTA);
        }

    }
}
