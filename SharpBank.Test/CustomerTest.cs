﻿using NUnit.Framework;
using SharpBank.Entity;
using SharpBank.Enums;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account((int)EnumTypeAccount.Checking);
            Account savingsAccount = new Account((int)EnumTypeAccount.Savings);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account((int)EnumTypeAccount.Savings));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account((int)EnumTypeAccount.Savings));
            oscar.OpenAccount(new Account((int)EnumTypeAccount.Checking));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account((int)EnumTypeAccount.Savings));
            oscar.OpenAccount(new Account((int)EnumTypeAccount.Checking));
            oscar.OpenAccount(new Account((int)EnumTypeAccount.MaxiSavings));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTransferBetweenAccounts_True()
        {
            //Arrange
            var accountBase = new Account((int)EnumTypeAccount.Savings);
            var accountDestiny = new Account((int)EnumTypeAccount.Checking);
            accountBase.Deposit(1000);
            var oscar = new Customer("Oscar")
                .OpenAccount(accountBase);
            oscar.OpenAccount(accountDestiny);
            
            //Act
            var result = oscar.TransferBetweenAccounts(accountBase, accountDestiny, 500.00);

            //Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public void TestTransferBetweenAccounts_False()
        {
            //Arrange
            var accountBase = new Account((int)EnumTypeAccount.Savings);
            var accountDestiny = new Account((int)EnumTypeAccount.Checking);
            accountBase.Deposit(400);
            var oscar = new Customer("Oscar")
                .OpenAccount(accountBase);
            oscar.OpenAccount(accountDestiny);

            //Act
            var result = oscar.TransferBetweenAccounts(accountBase, accountDestiny, 500.00);

            //Assert
            Assert.AreEqual(false, result);
        }
    }
}
